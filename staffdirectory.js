var express = require('express');
var fs = require('fs');

var app = express();

app.set('port', process.env.PORT || 3000);

var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

var formidable = require('formidable');

var cookieParser = require('cookie-parser');
app.use(cookieParser());


app.get('/', function (req, res) {

    var employees = JSON.parse(fs.readFileSync(__dirname + "/employees.json"));
    var sortOrder;
    if (req.query.sort == "ascending") {
        employees.sort(function (a, b) { return a.lname > b.lname });
        sortOrder = "descending";
    }
    else if (req.query.sort == "descending") {
        employees.sort(function (a, b) { return a.lname < b.lname });
        sortOrder = "default"
    } else {
        sortOrder = "ascending"
    }

    var data = {
        // employees: true,
        employees: employees,
        sortOrder: sortOrder
    };

    res.render('home', data);
});

app.post('/remove', function (req, res) {
    var employees = JSON.parse(fs.readFileSync(__dirname + "/employees.json"));

    if (req.body.select) {

        var selectToRemove;
        if (Array.isArray(req.body.select)) {
            selectToRemove = req.body.select;
        } else {
            selectToRemove = [req.body.select];
        }
        var restofEmployees = employees.filter(function (employee) {
            for (var i = 0; i < selectToRemove.length; i++) {
                if ("" + selectToRemove[i] == "" + employee.id) {
                    return false;
                }
            }
            return true;
        });
        fs.writeFileSync(__dirname + "/employees.json", JSON.stringify(restofEmployees));
    }
    else{
        fs.writeFileSync(__dirname + "/employees.json", JSON.stringify(employees));
    }

    res.redirect('/');
});

app.get('/add', function (req, res) {
    var cookies = [];
    if (req.cookies["cookies for incompleted entry"]) {
        var cookieValue = req.cookies["cookies for incompleted entry"];
        res.render('add', cookieValue);
        
    }
    else {
        res.render('add');
    }
    
});

app.post('/add', function (req, res) {
    var employees = JSON.parse(fs.readFileSync(__dirname + "/employees.json"));
    
    var newId = 0;
    for (var employee of employees) {
        if (employee.id >= newId) {
            newId = employee.id + 1;
        }
    }

    var employee = {
        id: newId,
        lname: req.body.newlastname,
        fname: req.body.newfirstname,
        email: req.body.newemail,
        ph: req.body.newphonenumber,
        address: req.body.newaddress,
        title: req.body.newjobtitle,
    }

    // assume form is complete
    var emptyProp = false;

    // check if each property is filled. If one form property is not filled, 
    // change emptyProp to true
    for (var property in employee) {
        if (employee[property] == '') {
            emptyProp = true;
            break;  
        }
    }

    // create cookies if there is empty property, otherwise
    // save information in employees.json
    var employeeName = "cookies for incompleted entry";
    if (emptyProp) {
        //create cookie
        res.cookie(employeeName, employee);
        res.redirect('/add');
    }
    else {
        // update employees.json file
        employees.push(employee);
        fs.writeFileSync(__dirname + "/employees.json", JSON.stringify(employees));
        
        // clear cookie
        res.clearCookie(employeeName);
        // redirect back to homepage
        res.redirect('/');   
    }

    
});

app.post('/clear', function (req, res) {
    
    if(req.cookies["cookies for incompleted entry"]){
    res.clearCookie("cookies for incompleted entry");
    }
    res.redirect('/add');  
});

app.use(express.static(__dirname + "/public"));

app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});
